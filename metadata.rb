maintainer       "Craig Stewart"
maintainer_email "craig.stewart@technophobia.com"
license          "All rights reserved"
description      "Installs/Configures tp-apache"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.0.8"
name     'tp-apache'
