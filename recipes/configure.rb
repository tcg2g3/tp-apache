#
# Cookbook Name:: tp-apache
# Recipe:: configure
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# create site directory layout

%w{non-secure locking}.each do |dir|
   directory node[:apache][:path]+"/"+node[:apache][:servername]+"/#{dir}" do
      mode "0775"
      owner "www-data"
      group "www-data"
      action :create
      recursive true
   end
end

%w{logs htdocs}.each do |dir|
   directory node[:apache][:path]+"/"+node[:apache][:servername]+"/non-secure/#{dir}" do
      mode "0775"
      owner "www-data"
      group "www-data"
      action :create
      recursive true
   end
end

# ports.conf
template "/etc/apache2/ports.conf" do
  source "ports.conf.erb"
  owner "root"
  group "root"
  mode "0644"
  notifies :restart, resources(:service => "apache2")
end

# site configuration
template "/etc/apache2/sites-available/"+node[:apache][:servername] do
  source "site.erb"
  owner "root"
  group "root"
  mode "0644"
end

# symlink site configuration from sites-available to sites-enabled
link "/etc/apache2/sites-enabled/"+node[:apache][:servername] do
  to "/etc/apache2/sites-available/"+node[:apache][:servername]
  notifies :restart, resources(:service => "apache2")
end
