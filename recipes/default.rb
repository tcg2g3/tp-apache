#
# Cookbook Name:: tp-apache
# Recipe:: default
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# Install Apache2
package "apache2" do
  action :install
end

# enable apache2 service and start it
service "apache2" do
  supports [:stop, :start, :restart, :reload]
  #action [:start, :enable]
  action [:nothing]
end

if node[:lsb][:codename] == 'jessie'
  # Remove link to default site config
link "/etc/apache2/sites-enabled/000-default.conf" do
  action :delete
  notifies :restart, resources(:service => "apache2")
end
else
# Remove link to default site config
link "/etc/apache2/sites-enabled/000-default" do
  action :delete
  notifies :restart, resources(:service => "apache2")
end
end




# Enable ReWrite Module
link "/etc/apache2/mods-enabled/rewrite.load" do
  to "/etc/apache2/mods-available/rewrite.load"
  notifies :restart, resources(:service => "apache2")
end

# Install CronoLog
package "cronolog" do
  action :install
end
