#
# Cookbook Name:: tp-apache
# Recipe:: php5
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# Install php5
package "php5" do
  action :install
end

# install mysql-client
package "mysql-client" do
  action :install
end

package "php5-mysql" do
  action :install
end

# restart apache2 service
service "apache2" do
  action [:start, :enable]
end

template "/etc/php5/apache2/php.ini" do
  source "php.ini.erb"
  owner "root"
  group "root"
  mode "0644"
end
