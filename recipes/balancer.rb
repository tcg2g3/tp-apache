#
# Cookbook Name:: tp-apache
# Recipe:: balancer
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# ports.conf
template "/etc/apache2/ports.conf" do
  source "ports.conf.ssl.erb"
  owner "root"
  group "root"
  mode "0644"
  notifies :restart, resources(:service => "apache2")
end

%w{secure non-secure locking}.each do |dir|
   directory node[:apache][:path]+"/"+node[:apache][:servername]+"/#{dir}" do
      mode "0775"
      owner "www-data"
      group "www-data"
      action :create
      recursive true
   end
end

%w{logs htdocs}.each do |dir|
   directory node[:apache][:path]+"/"+node[:apache][:servername]+"/non-secure/#{dir}" do
      mode "0775"
      owner "www-data"
      group "www-data"
      action :create
      recursive true
   end
   directory node[:apache][:path]+"/"+node[:apache][:servername]+"/secure/#{dir}" do
      mode "0775"
      owner "www-data"
      group "www-data"
      action :create
      recursive true
   end
end

# enable mod_ssl
link "/etc/apache2/mods-enabled/ssl.load" do
  to "/etc/apache2/mods-available/ssl.load"
  notifies :restart, resources(:service => "apache2")
end

# enable mod_proxy
link "/etc/apache2/mods-enabled/proxy.load" do
  to "/etc/apache2/mods-available/proxy.load"
  notifies :restart, resources(:service => "apache2")
end
link "/etc/apache2/mods-enabled/proxy_ajp.load" do
  to "/etc/apache2/mods-available/proxy_ajp.load"
  notifies :restart, resources(:service => "apache2")
end
link "/etc/apache2/mods-enabled/proxy_http.load" do
  to "/etc/apache2/mods-available/proxy_http.load"
  notifies :restart, resources(:service => "apache2")
end
link "/etc/apache2/mods-enabled/proxy_balancer.load" do
  to "/etc/apache2/mods-available/proxy_balancer.load"
  notifies :restart, resources(:service => "apache2")
end

# site configuration
template "/etc/apache2/sites-available/"+node[:apache][:servername] do
  source "balancer.erb"
  owner "root"
  group "root"
  mode "0644"
  variables(:serveralias => node[:apache][:serveralias],
            :blockip => node[:apache][:blockip],
            :balancerhost => node[:apache][:balancerhost])
end

# symlink site configuration from sites-available to sites-enabled
link "/etc/apache2/sites-enabled/"+node[:apache][:servername] do
  to "/etc/apache2/sites-available/"+node[:apache][:servername]
  notifies :restart, resources(:service => "apache2")
end
