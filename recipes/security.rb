#
# Cookbook Name:: tp-apache
# Recipe:: security
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
# default server hardening
if node[:lsb][:codename] == 'jessie'
  package "libapache2-mod-security2" do
    action :install
  end
  
  file "/etc/apache2/mods-available/mod-security.load" do
    content "LoadFile libxml2.so.2 \n LoadModule security2_module /usr/lib/apache2/modules/mod_security2.so"
  end
  file "/etc/apache2/mods-available/mod-unique-id.load" do
    content "LoadModule unique_id_module /usr/lib/apache2/modules/mod_unique_id.so"
  end
else
  package "libapache-mod-security" do
    action :install
  end
end

# enable mod_security
link "/etc/apache2/mods-enabled/mod-security.load" do
  to "/etc/apache2/mods-available/mod-security.load"
#  notifies :restart, resources(:service => "apache2")
end
#enable mod-unique-id
link "/etc/apache2/mods-enabled/mod-unique-id.load" do
  to "/etc/apache2/mods-available/mod-unique-id.load"
end
# enable mod_headers
link "/etc/apache2/mods-enabled/headers.load" do
  to "/etc/apache2/mods-available/headers.load"
#  notifies :restart, resources(:service => "apache2")
end

# enable mod_headers
link "/etc/apache2/mods-enabled/auth_digest.load" do
  to "/etc/apache2/mods-available/auth_digest.load"
#  notifies :restart, resources(:service => "apache2")
end

# site configuration
if node[:lsb][:codename] == 'jessie'
# enabled by default
  template "/etc/apache2/conf-available/security" do
    source "security.erb"
    owner "root"
    group "root"
    mode "0644"
    notifies :restart, resources(:service => "apache2")
  end
else
  template "/etc/apache2/conf.d/security" do
    source "security.erb"
    owner "root"
    group "root"
    mode "0644"
    notifies :restart, resources(:service => "apache2")
  end
end
